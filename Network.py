import numpy as np
# from sklearn.model_selection import KFold
from numpy import exp
from sklearn.metrics import log_loss

def loss(y_pred, y):
    return (y_pred - y)**2

def loss_derivative(y_pred, y):
    return 2*(y_pred - y)

# activation function
def sigmoid(z):
    z = z.astype(np.float)
    return 1.0/(1.0 + exp(-z))

def sigmoid_derivative(z):
    return sigmoid(z)*(1.0 - sigmoid(z))

def RELU(z):
    r = z.shape[0]
    c = z.shape[1]
    return np.array([max(0, i[0]) for i in z]).reshape(r, c)

def RELU_derivative(z):
    r = z.shape[0]
    c = z.shape[1]
    return np.array([1 if x > 0 else 0 for x in z]).reshape(r, c)


class Layer:
    # num_neuron: number of neurons in the layer
    # next_num_neuron: number of neurons in the next layer
    # bias: chi so thien thien vi
    # randn(x, y): x - row, y - column
    def __init__(self, num_neuron, pre_num_neuron):
        self.num_neuron = num_neuron
        
        if pre_num_neuron != 0:
            # bias
            self.bias = np.random.randn(num_neuron, 1)
            # weights of layer
            self.weights = np.random.randn(num_neuron, pre_num_neuron)
        else:
            self.bias = None
            self.weights = None

    def __str__(self):
        return f"""
            {self.num_neuron}
            Bias: 
            {self.bias} 
            Weight: 
            {self.weights}
        """
        
class Network:
    # sizes: list of integers each of which represent number of neurons in the layer
    # [4, 10, 4]
    def __init__(self, sizes, activation_function = sigmoid, activation_function_d = sigmoid_derivative):
        self.num_layer = len(sizes)
        self.sizes = sizes
        self.layers = [Layer(sizes[i], sizes[i-1]) for i in range(1, self.num_layer)]
        self.layers.insert(0, Layer(sizes[0], 0))
        
        # self.activation_function = sigmoid
        # self.activation_function_d = sigmoid_derivative
        
        self.activation_function = activation_function
        self.activation_function_d = activation_function_d
        
    def feed_forward(self, a):
        # Start with a - attribute of data
        # In each layer a is activation
        # Return result of network
        activation = np.array([a]).T
        for i in range(1, self.num_layer):
            layer = self.layers[i]
            activation = self.activation_function(np.dot(layer.weights, activation) + layer.bias)
        return activation
    
    def _feed_forward(self, a):
        # Save activation and z for each layer
        # Return activation and z of last layer
        self.layers[0].activation = activation = np.array([a]).T
        self.layers[0].z = None
        
        for i in range(1, self.num_layer):
            layer = self.layers[i]
            layer.z = z = np.dot(layer.weights, activation) + layer.bias
            
            layer.activation = activation = self.activation_function(z)
        return activation, z
    
    def _release_memory(self):
        for i in range(self.num_layer):
            layer = self.layers[i]
            del layer.z
            del layer.activation
            
    def backpropagation(self, x, y):
        # Feed forward
        activation, z = self._feed_forward(x)
        
        Y = np.array([y]).T
        # Calculate at last layer
        ## delta for bias        

        delta = loss_derivative(activation, Y) * self.activation_function_d(z)
        
        delta_b = [delta] 
        
        ## delta for weight
        delta_w = [np.dot(delta, self.layers[-2].activation.T)]

        # back way
        for i in range(2, self.num_layer):
            z = self.layers[-i].z

            delta =  np.dot(self.layers[-i + 1].weights.T, delta)
            delta *= self.activation_function_d(z)
            
            delta_b.insert(0, delta)
            
            delta_w.insert(0, np.dot(delta, self.layers[-i-1].activation.T))
            # print(delta.shape, self.layers[-i-1].activation.T.shape)
            # print(delta_w[0].shape)
        self._release_memory()
        
        return delta_b, delta_w
    
    def update_network(self, data, eta):
        x, y = data[0]
        
        n = len(data)
        nabla_b, nabla_w = self.backpropagation(x, y)
        
        for i in range(1, n):
            x, y = data[i]
        
            nabla_b, nabla_w = self.backpropagation(x, y)
            
            # delta_b += delta_b_tmp
            # delta_w += delta_w_tmp
            # Chua tu layer 2 den cuoi
            # for i in range(len(delta_b)):
            #     # nabla_b[i] += delta_b[i]
            #     # nabla_w[i] += delta_w[i]
            
            delta_b, delta_w = self.backpropagation(x, y)
            # nabla_b = [nb+dnb for nb, dnb in zip(nabla_b, delta_b)]
            # nabla_w = [nw+dnw for nw, dnw in zip(nabla_w, delta_w)]
        
            nabla_b += delta_b
            nabla_w += delta_w
            
        for i in range(1, self.num_layer):
            layer = self.layers[i]

            layer.weights = layer.weights - (eta/n) * delta_w[i-1]
            layer.bias = layer.bias - (eta/n) * delta_b[i-1]
        
    def SGD(self, training_data, epochs, X_test=[], y_test=[], eta=0.1, n_splits=5):
        b = int(len(training_data)/n_splits)
        
        for z in range(epochs):
            # kFold = KFold(n_splits=n_splits, shuffle=True, random_state=i)
            # print(training_data)
            # for train, test in kFold.split(training_data):
            #     print(test)
            low = 0
            high = b
            for i in range(n_splits):
                np.random.shuffle(training_data[low:high])
                low = high
                if (i < n_splits - 1):
                    high += b
                else:
                    high = len(training_data)
                
            self.update_network(training_data, eta)
            n = len(X_test)
            if (n and len(y_test)):
                print(f'''
                Loop: {z}
                {self.evaluate(X_test, y_test)} / {n}
                    ''')
            
    def evaluate(self, X_test, y_test):
        count = 0
        y_preds = []
        for x in X_test:
            y_preds.append(self.feed_forward(x))

        for y, y_pred in zip(y_test, y_preds):
            if np.argmax(y) == np.argmax(y_pred):
                count += 1

        return count
    
    def __str__(self):
        str1 = ""
        for layer in self.layers:
            str1 += layer.__str__()
        return str1


if __name__ == "__main__":
    network = Network([4, 5, 4])
    print(network)
